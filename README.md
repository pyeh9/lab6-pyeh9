# CSE 6230, Fall 2014: Lab 6: UVA/UVM and MPI+CUDA #

Part 1:
[pyeh9@jinx4 lab6-pyeh9]$ ./rev 16777216
n = 16777216 (~ 64.0 MiB)
Test 1: Using 'malloc' on CPU...
dbenchmarkReverseWithCopies: Testing...
==> Passed!

benchmarkReverseWithCopies: Timing...
Timer: gettimeofday
Timer resolution: ~ 1 us (?)
==> 53 trials took 2.00949 seconds.
==> Reversal with explicit copies: 0.037915 seconds (3.53997 effective GB/s)

Test 2: Using pinned cudaHostAlloc...
benchmarkReverseWithoutCopies: Testing...
==> Passed!

benchmarkReverseWithoutCopies: Timing...
Timer: gettimeofday
Timer resolution: ~ 1 us (?)
==> 122 trials took 2.00693 seconds.
==> Reversal without explicit copies: 0.0164503 seconds (8.15899 effective GB/s)

Summary: unpinned 3.53997 GB/s vs pinned 8.15899 

Part 2:
[pyeh9@jinx3 lab6-pyeh9]$ ./mgpu-dma 16777216
n = 16777216 (~ 64.0 MiB)

Timer: gettimeofday
Timer resolution: ~ 1 us (?)
Detected 2 GPUs.
Test 1: Buffering GPU-to-GPU copy using CPU memory...
  [17 trials]
==> GPU-to-GPU copy, buffered through CPU memory: 0.0599971 seconds (2.23707 GB/s)

Test 2: Direct GPU-to-GPU copy...
  [23 trials]
==> Direct GPU-to-GPU copy: 0.0448323 seconds (2.99378 GB/s)

Part 3:
(BLAS)
jinx19
jinx20
========================================
Problem dimension: n = 4096
Number of MPI ranks: 2
Number of trials: 3
Time per trial (max over all processes): 0.651172 seconds
Computation time per trial (max over all processes): 0.612951 seconds
Effective performance: 211.1 GFLOP/s
========================================

(CUDA)
jinx19
jinx20
========================================
Problem dimension: n = 4096
Number of MPI ranks: 2
Number of trials: 6
Time per trial (max over all processes): 0.180169 seconds
Computation time per trial (max over all processes): 0.139715 seconds
Effective performance: 762.8 GFLOP/s
========================================

Part 4:
At first I implemented pinned memory and assumed UVA and passed into cublasSgemm() the pinned memory pointers, but I got a huge slowdown (~15 GFLOP/s). Following the advice on Piazza, I implemented pinned memory but used explicit transfers. 

The following changes were done:
In mm_create():
- malloc() was replaced with cudaHostAlloc() 
- return A_pinned

In mm_free():
- free() was replaced with cudaHostFree()

In mm_local():
- cudaMemcpy() calls modified with the type of transfer that was occuring. 

Running the program with these changes:
jinx19
jinx20
========================================
Problem dimension: n = 4096
Number of MPI ranks: 2
Number of trials: 6
Time per trial (max over all processes): 0.173671 seconds
Computation time per trial (max over all processes): 0.126276 seconds
Effective performance: 791.4 GFLOP/s
========================================

Observed a speedup from 762.8 GFLOP/s to 791.4 GFLOP/s
